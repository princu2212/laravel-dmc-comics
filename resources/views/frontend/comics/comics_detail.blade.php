@extends('frontend.main')
@section('title')
    Comics Detail
@endsection
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="{{ route('index') }}"><i class="fa fa-home"></i> Home</a>
                        <span>{{ $comics->title }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Anime Section Begin -->
    <section class="anime-details spad">
        <div class="container">
            <div class="anime__details__content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="anime__details__pic set-bg"
                            data-setbg="{{ !empty($comics->image) ? url('upload/comics/' . $comics->image) : url('upload/no_image.jpg') }}">
                            @php
                                $comment = App\Models\Comment::where('comics_id', $comics->id)->get();
                            @endphp
                            <div class="comment"><i class="fa fa-comments"></i> {{ count($comment) }}</div>
                            <div class="view count{{ $comics['id'] }}"><i class="fa fa-download"></i> {{ $comics['count'] }}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="anime__details__text">
                            <div class="anime__details__title">
                                <h3>{{ $comics->title }}</h3>
                            </div>
                            <p>{!! $comics->description !!}</p>
                            {!! Share::page(url($comics->title))->facebook()->twitter()->whatsapp() !!}</p>
                            <div class="anime__details__widget">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <ul>
                                            <li><span>Type:</span> {{ $comics->category }}</li>
                                            <li><span>Published:</span>
                                                {{ Carbon\Carbon::parse($comics->created_at)->format('d M, Y') }}</li>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="anime__details__btn">
                                <a href="{{ '/upload/file/' . $comics->link }}" target="_blank" class="watch-btn"
                                    data-id="{{ $comics->id }}"><span>Download Now</span></i></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="anime__details__review">
                        <div class="section-title">
                            <h5>Comments</h5>
                        </div>
                        @forelse($comics->comments as $item)
                            <div class="anime__review__item">
                                <div class="anime__review__item__pic">
                                    <img src="{{ asset('dmc.jpg') }}" alt="">
                                </div>
                                <div class="anime__review__item__text">
                                    <h6>Annonymous -
                                        <span>{{ Carbon\Carbon::parse($item->created_at)->format('d M, Y') }}</span>
                                    </h6>
                                    <p>{{ $item->comment }}</p>
                                </div>
                            </div>
                        @empty
                            <h6 class="text-white">No Comments Yet.</h6>
                        @endforelse
                    </div>
                    <div class="anime__details__form">
                        <div class="section-title">
                            <h5>Your Comment</h5>
                        </div>
                        <form action="{{ route('comment.store') }}" method="post">
                            <input type="hidden" name="comics_id" value="{{ $comics->id }}">
                            @csrf
                            <textarea placeholder="Your Comment" name="comment"></textarea>
                            <button type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="anime__details__sidebar">
                        <div class="section-title">
                            <h5>you might like...</h5>
                        </div>
                        @php
                            $top_views = App\Models\Comic::orderBy('count', 'DESC')
                                ->limit(5)
                                ->get();
                        @endphp
                        @foreach ($top_views as $item)
                            <a href="{{ route('comic.details', $item->id) }}">
                                <div class="product__sidebar__view__item set-bg"
                                    data-setbg="{{ !empty($item->image) ? url('upload/comics/' . $item->image) : url('upload/no_image.jpg') }}">
                                    <div class="view"><i class="fa fa-eye"></i> {{ $item->count }}</div>
                                    <h5 class="text-white">{{ $item->title }}</h5>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Anime Section End -->

    {{-- jQuery for download --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.watch-btn').click(function() {
                var comic_id = $(this).attr('data-id');

                $.ajax({
                    type: 'GET',
                    url: "{{ route('counter') }}",
                    data: {
                        id: comic_id
                    },
                    success: function(data) {
                        $(".count" + comic_id).text(data.comicCount);
                        $(".total-count").text(data.totalCount);
                    }
                });
            });
        });
    </script>

@endsection
