<!-- Header Section Begin -->
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="header__logo">
                    <a href="#">
                        <img src="{{ asset('dmc.jpg') }}" alt="header__logo" height="65px">
                    </a>
                </div>
            </div>
            <div class="col-lg-10">
                <div class="header__nav">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            @php
                                $route = Route::current()->getName();
                            @endphp
                            <li class="{{ $route == 'index' ? 'active' : '' }}"><a
                                    href="{{ route('index') }}">Homepage</a></li>
                            <li class="{{ $route == 'all.comic' ? 'active' : '' }}"><a
                                    href="{{ route('all.comic') }}">Comics</a></li>
                            <li class="{{ $route == 'about' ? 'active' : '' }}"><a href="{{ route('about') }}">About
                                    Us</a></li>
                            <li class="{{ $route == 'contact' ? 'active' : '' }}"><a
                                    href="{{ route('contact') }}">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            {{-- <div class="col-lg-2">
                @if (Route::has('login'))
                    <div class="header__right">
                        <a href="#" class="search-switch"><span class="icon_search"></span></a>
                        <a href="{{ route('login') }}"><span class="icon_profile"></span></a>
                    </div>
                @endif
            </div> --}}
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>
<!-- Header End -->
