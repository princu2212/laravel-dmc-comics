<!-- Comics Section Begin -->
<section class="product spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="trending__product">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <div class="section-title">
                                <h4>New Comics</h4>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="btn__all">
                                <a href="{{ route('all.comic') }}" class="primary-btn">View All <span
                                        class="arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($comics as $item)
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg"
                                        data-setbg="{{ !empty($item->image) ? url('upload/comics/' . $item->image) : url('upload/no_image.jpg') }}">
                                        @php
                                            $comment = App\Models\Comment::where('comics_id', $item->id)->get();
                                        @endphp
                                        <div class="comment"><i class="fa fa-comments"></i> {{ count($comment) }}
                                        </div>
                                        <div class="view"><i class="fa fa-download"></i> {{ $item->count }}</div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li>{{ $item->category }}</li>
                                        </ul>
                                        <h5><a
                                                href="{{ route('comic.details', $item->id) }}">{{ $item->title }}</a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-8">
                <div class="product__sidebar">
                    <div class="product__sidebar__view">
                        <div class="section-title">
                            <h5>Top Views</h5>
                        </div>
                        <div class="filter__gallery">
                            @php
                                $top_views = App\Models\Comic::orderBy('count', 'DESC')
                                    ->limit(5)
                                    ->get();
                            @endphp
                            @foreach ($top_views as $item)
                                <a href="{{ route('comic.details', $item->id) }}">
                                    <div class="product__sidebar__view__item set-bg mix day years"
                                        data-setbg="{{ !empty($item->image) ? url('upload/comics/' . $item->image) : url('upload/no_image.jpg') }}">
                                        <div class="view"><i class="fa fa-eye"></i> {{ $item->count }}</div>
                                        <h5 class="text-white">{{ $item->title }}</h5>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Comics Section End -->
