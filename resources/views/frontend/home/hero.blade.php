<section class="hero">
    <div class="container">
        <div class="hero__slider owl-carousel">
            @foreach ($comics as $item)
                <div class="hero__items set-bg"
                    data-setbg="{{ !empty($item->image) ? url('upload/comics/' . $item->image) : url('upload/no_image.jpg') }}">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <div class="label">Adventure</div>
                                <h2>{{ $item->title }}</h2>
                                <a href="{{ '/upload/file/' . $item->link }}" target="_blank"
                                    class="watch-btn"><span>Download Now</span></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
