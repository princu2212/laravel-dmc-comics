@extends('frontend.main')
@section('title')
    Contact Us
@endsection
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="{{ route('index') }}"><i class="fa fa-home"></i> Home</a>
                        <span>Contact Us</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Anime Section Begin -->
    <section class="anime-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-8">
                    <div class="anime__details__review">
                        <div class="section-title">
                            <h5>Contact Us</h5>
                        </div>
                        <div class="anime__details__widget">
                            <ul>
                                <li><span>Name:</span> {{ $contact->name }}</li>
                                <li><span>Email:</span> {{ $contact->email }}</li>
                                <li><span>Mobile:</span> {{ $contact->mobile }}</li>
                                <li><span>Address:</span> {{ $contact->address }}</li>
                            </ul>
                            <div class="section-title">
                                <h5>Follow Us</h5>
                            </div>
                            <ul class="social-links">
                                <a href="{{ $contact->facebook }}" target="_blank"><i
                                        class="fa-brands fa-2x fa-facebook"></i></a>
                                <a href="{{ $contact->instagram }}" target="_blank"><i
                                        class="fa-brands fa-2x fa-instagram"></i></a>
                                <a href="{{ $contact->twitter }}" target="_blank"><i
                                        class="fa-brands fa-2x fa-twitter"></i></a>
                                <a href="{{ $contact->whatsapp }}" target="_blank"><i
                                        class="fa-brands fa-2x fa-whatsapp"></i></a>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-8">
                    <div class="anime__details__form">
                        <div class="section-title">
                            <h5>Any Suggestion</h5>
                        </div>
                        <form action="{{ route('message.store') }}" method="post">
                            @csrf
                            <input type="text" name="name" placeholder="Your Name">
                            <input type="email" name="email" placeholder="Your Email">
                            <textarea placeholder="Your Comment" name="comment"></textarea>
                            <button type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
    </section>
    <!-- Anime Section End -->
@endsection
