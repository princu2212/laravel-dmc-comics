<!-- Footer Section Begin -->
<footer class="footer">
    <div class="page-up">
        <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="footer__logo">
                    <a href="#"><img src="img/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footer__nav">
                    <ul>
                        @php
                            $route = Route::current()->getName();
                        @endphp
                        <li class="{{ $route == 'index' ? 'active' : '' }}"><a href="{{ route('index') }}">Homepage</a>
                        </li>
                        <li class="{{ $route == 'all.comic' ? 'active' : '' }}"><a
                                href="{{ route('all.comic') }}">Comics</a></li>
                        <li class="{{ $route == 'about' ? 'active' : '' }}"><a href="{{ route('about') }}">About
                                Us</a></li>
                        <li class="{{ $route == 'contact' ? 'active' : '' }}"><a href="{{ route('contact') }}">Contact
                                Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <p>Copyright &copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->
