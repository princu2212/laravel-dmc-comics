@extends('frontend.main')
@section('title')
    Dark Magic Comics | Home
@endsection
@section('content')
    <!-- Hero Section Begin -->
    @include('frontend.home.hero')
    <!-- Hero Section End -->

    <!-- Comics Section Begin -->
    @include('frontend.home.comics')
    <!-- Comics Section End -->
@endsection
