@extends('frontend.main')
@section('title')
    About Us
@endsection
@section('content')
    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="{{ route('index') }}"><i class="fa fa-home"></i> Home</a>
                        <span>About Us</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Anime Section Begin -->
    <section class="anime-details spad">
        <div class="container">
            <div class="anime__details__content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="anime__details__pic about_page set-bg"
                            data-setbg="{{ !empty($about->image) ? url('upload/about/' . $about->image) : url('upload/no_image.jpg') }}">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="anime__details__text">
                            <div class="anime__details__title">
                                <h3>{{ $about->title }}</h3>
                            </div>
                            <p>{!! $about->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Anime Section End -->
@endsection
