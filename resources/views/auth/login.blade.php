@extends('frontend.main')
@section('title')
    DMC | Login
@endsection
@section('content')
    <section class="login spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="login__form">
                        <!-- Session Status -->
                        <x-auth-session-status class="mb-4 text-white" :status="session('status')" />

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4 text-white" :errors="$errors" />
                        <h3>Login</h3>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="input__item">
                                <input type="email" name="email" placeholder="Email address" required autofocus>
                                <span class="icon_mail"></span>
                            </div>
                            <div class="input__item">
                                <input type="password" name="password" placeholder="Password" required>
                                <span class="icon_lock"></span>
                            </div>
                            <button type="submit" class="site-btn">Login Now</button>
                            @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}" class="forget_pass">Forgot Your Password?</a>
                            @endif
                        </form>
                    </div>
                </div>
                {{-- <div class="col-lg-6">
                    <div class="login__register">
                        <div class="register__form px-0">
                            <!-- Validation Errors -->
                            <x-auth-validation-errors class="mb-4 text-white" :errors="$errors" />
                            <h3>Register</h3>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="input__item">
                                    <input type="text" placeholder="Your Name" name="name" :value="old('name')"
                                        required autofocus>
                                    <span class="icon_profile"></span>
                                </div>

                                <div class="input__item">
                                    <input type="email" placeholder="Email address" name="email" :value="old('email')"
                                        required>
                                    <span class="icon_mail"></span>
                                </div>

                                <div class="input__item">
                                    <input type="password" placeholder="Password" name="password" required
                                        autocomplete="new-password">
                                    <span class="icon_lock"></span>
                                </div>

                                <div class="input__item">
                                    <input type="password" placeholder="Confirm Password" name="password_confirmation"
                                        required>
                                    <span class="icon_lock"></span>
                                </div>

                                <button type="submit" class="site-btn">Register</button>
                            </form>
                        </div>
                        <h3>Dont’t Have An Account?</h3>
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="primary-btn">Register Now</a>
                        @endif
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
@endsection

{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"
                    required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full" type="password" name="password" required
                    autocomplete="current-password" />
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox"
                        class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                        name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900"
                        href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout> --}}
