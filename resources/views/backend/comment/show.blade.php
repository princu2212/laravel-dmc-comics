@extends('backend.main')
@section('title')
    Show | Comment
@endsection
@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title">Show Comment</h4>
                            <div class="row mb-3">
                                <label for="title" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                    <input name="title" value="{{ $comics->title }}" class="form-control" type="text"
                                        disabled>
                                </div>
                            </div>
                            <!-- end row -->

                            <div class="row mb-3">
                                <label for="comment" class="col-sm-2 col-form-label">Comments</label>
                                <div class="col-sm-10">
                                    <textarea id="elm1" name="comment" disabled>{{ $comment->comment }}</textarea>
                                </div>
                            </div>
                            <!-- end row -->
                            <a href="{{ route('comment.index') }}" class="btn btn-info waves-effect waves-light">Back</a>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
@endsection
