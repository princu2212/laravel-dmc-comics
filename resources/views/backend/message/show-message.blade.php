@extends('backend.main')
@section('title')
    Show | Message
@endsection
@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title">Show Message</h4>
                            <div class="row mb-3">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input name="name" value="{{ $message->name }}" class="form-control" type="text"
                                        disabled>
                                </div>
                            </div>
                            <!-- end row -->

                            <div class="row mb-3">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input name="email" value="{{ $message->email }}" class="form-control" type="text"
                                        disabled>
                                </div>
                            </div>
                            <!-- end row -->

                            <div class="row mb-3">
                                <label for="comment" class="col-sm-2 col-form-label">Message</label>
                                <div class="col-sm-10">
                                    <textarea id="elm1" name="comment">{{ $message->comment }}</textarea>
                                </div>
                            </div>
                            <!-- end row -->
                            <a href="{{ route('message') }}" class="btn btn-info waves-effect waves-light">Back</a>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
@endsection
