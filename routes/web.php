<?php

use App\Http\Controllers\backend\AboutController;
use App\Http\Controllers\backend\ComicController;
use App\Http\Controllers\backend\CommentController;
use App\Http\Controllers\backend\ContactController;
use App\Http\Controllers\backend\MessageController;
use App\Http\Controllers\frontend\IndexController;
use Illuminate\Support\Facades\Route;

// Frontend All Routes
Route::controller(IndexController::class)->group(function () {
    Route::get('/', 'index')->name('index');
    Route::get('/comic-details/{id}', 'comicDetails')->name('comic.details');
    Route::get('/all/comic', 'allComic')->name('all.comic');
    Route::get('/aboutus', 'about')->name('about');
    Route::get('/contactus', 'contact')->name('contact');
});

Route::get('/dashboard', function () {
    return view('backend.index');
})->middleware(['auth'])->name('dashboard');

// Backend All Routes
Route::controller(AboutController::class)->group(function () {
    Route::get('/about/create', 'create')->name('about.create');
    Route::put('/about/update/{id}', 'update')->name('about.update');
});

Route::controller(ComicController::class)->group(function () {
    Route::get('/comic/create', 'create')->name('comic.create');
    Route::post('/comic/store', 'store')->name('comic.store');
    Route::get('/comic/show', 'show')->name('comic.show');
    Route::get('/comic/edit/{id}', 'edit')->name('comic.edit');
    Route::put('/comic/update/{id}', 'update')->name('comic.update');
    Route::delete('/comic/destroy/{id}', 'destroy')->name('comic.destroy');
    Route::get('counter', 'counter')->name('counter');
});

Route::controller(ContactController::class)->group(function () {
    Route::get('/contact/create', 'create')->name('contact.create');
    Route::put('/contact/update/{id}', 'update')->name('contact.update');
});

Route::controller(MessageController::class)->group(function () {
    Route::get('/message', 'index')->name('message');
    Route::post('/message/store', 'storeMessage')->name('message.store');
    Route::get('/message/show/{id}', 'show')->name('message.show');
    Route::delete('/message/destroy/{id}', 'destroy')->name('message.destroy');
});

Route::controller(CommentController::class)->group(function () {
    Route::get('/comments', 'index')->name('comment.index');
    Route::post('/comment/store', 'store')->name('comment.store');
    Route::get('/comment/show/{id}', 'show')->name('comment.show');
    Route::delete('/comment/destroy/{id}', 'destroy')->name('comment.destroy');
});


require __DIR__ . '/auth.php';
