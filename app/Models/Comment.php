<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['comics_id', 'comment'];

    public function comics()
    {
        return $this->belongsTo(Comic::class, 'comics_id', 'id');
    }
}
