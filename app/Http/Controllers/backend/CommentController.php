<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Comic;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        $comment = Comment::all();
        return view('backend.comment.index', compact('comment'));
    }

    public function store(Request $request)
    {
        $comics = Comic::where('id', $request->comics_id)->first();
        if ($comics) {
            Comment::create([
                'comics_id' => $comics->id,
                'comment' => $request->comment
            ]);
        }

        $notification = array(
            'message' => 'Comment Added Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        $comics = Comic::find($id);
        $comment = Comment::find($id);
        return view('backend.comment.show', compact('comment', 'comics'));
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);
        $comment->delete();

        $notification = array(
            'message' => 'Comment Deleted Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}
