<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        $message = Message::latest()->get();
        return view('backend.message.message', compact('message'));
    }

    public function storeMessage(Request $request)
    {
        $message = new Message();
        $message->name = $request->name;
        $message->email = $request->email;
        $message->comment = $request->comment;

        $message->save();

        $notification = array(
            'message' => 'Message Sent Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        $message = Message::find($id);
        return view('backend.message.show-message', compact('message'));
    }

    public function destroy($id)
    {
        $message = Message::find($id);
        $message->delete();

        $notification = array(
            'message' => 'Message Deleted Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('message')->with($notification);
    } // end method
}
