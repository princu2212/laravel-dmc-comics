<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Comic;
use Illuminate\Http\Request;

class ComicController extends Controller
{
    public function create()
    {
        return view('backend.comics.add');
    }

    public function store(Request $request)
    {
        $comics = new Comic();
        $comics->category = $request->category;
        $comics->title = $request->title;
        // $comics->slug = $request->strtolower(str_replace(' ', '-', $request->title));
        $comics->description = $request->description;

        if ($request->file('link')) {
            $file = $request->file('link');
            @unlink(public_path('upload/file/' . $comics->link));
            $filename = $file->getClientOriginalName();
            $file->move(public_path('upload/file'), $filename);
            $comics['link'] = $filename;
        }

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('upload/comics/' . $comics->image));
            $filename = hexdec(uniqid()) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('upload/comics'), $filename);
            $comics['image'] = $filename;
        }

        $comics->save();

        $notification = array(
            'message' => 'Comics Added Successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function show()
    {
        $comics = Comic::latest()->get();
        return view('backend.comics.view', compact('comics'));
    } // end method

    public function edit($id)
    {
        $comics = Comic::find($id);
        return view('backend.comics.edit', compact('comics'));
    } // end method

    public function update(Request $request, $id)
    {
        $comics = Comic::find($id);
        $comics->category = $request->category;
        $comics->title = $request->title;
        $comics->slug = $request->strtolower(str_replace(' ', '-', $request->title));
        $comics->description = $request->description;

        if ($request->file('link')) {
            $file = $request->file('link');
            @unlink(public_path('upload/file/' . $comics->link));
            $filename = $file->getClientOriginalName();
            $file->move(public_path('upload/file'), $filename);
            $comics['link'] = $filename;
        }

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('upload/comics/' . $comics->image));
            $filename = hexdec(uniqid()) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('upload/comics'), $filename);
            $comics['image'] = $filename;
        }

        $comics->save();

        $notification = array(
            'message' => 'Comics Updated Successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function destroy($id)
    {
        $comics = Comic::find($id);
        @unlink(public_path('upload/comics/' . $comics->image));
        @unlink(public_path('upload/file/' . $comics->link));
        $comics->delete();

        $notification = array(
            'message' => 'Comics Deleted Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('comic.show')->with($notification);
    } // end method

    public function counter(Request $request)
    {
        $counter = Comic::find($request->id);
        $counter->count = $counter->count + 1;
        $counter->save();

        $comicCount = $counter->count;

        $totalCount = Comic::sum('count');

        return response()->json([' comicCount' => $comicCount, 'totalCount' => $totalCount]);
    }
}
