<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Comic;
use App\Models\Contact;

class IndexController extends Controller
{
    public function index()
    {
        $comics = Comic::latest()->get();
        return view('frontend.index', compact('comics'));
    }

    public function comicDetails($id)
    {
        $comics = Comic::find($id);
        $totalCount = Comic::sum('count');

        // Social Media Share
        $shareComponent = \Share::page(
            'https://dmc.princeyadav.net/',
            'Your share text comes here',
        )
            ->facebook()
            ->twitter()
            ->whatsapp();

        $posts = Comic::get();
        return view('frontend.comics.comics_detail', compact('comics', 'totalCount', 'shareComponent', 'posts'));
    }

    public function allComic()
    {
        $comics = Comic::latest()->get();
        return view('frontend.comics.comics', compact('comics'));
    }

    public function about()
    {
        $about = About::find(1);
        return view('frontend.about.about', compact('about'));
    }

    public function contact()
    {
        $contact = Contact::find(1);
        return view('frontend.contact.contact', compact('contact'));
    }
}
